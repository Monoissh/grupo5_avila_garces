import socket
import base64

HEADER = 64
PORT = 3074
SERVER = '192.168.15.168'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)


def send(msg):
    message=msg.encode(FORMAT)
    
    # Cifrado
    msg_cifrado = message
    msg_cif64 = base64.b64encode(msg_cifrado)
    message = msg_cif64


    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))


send("MAC 00:0c:29:51:c1:2f")
input()
send("tamos listos")
input()
send(DISCONNECT_MESSAGE)

