import socket
import threading
import base64

HEADER = 64
PORT = 3074
SERVER = '192.168.15.168'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


server.bind(ADDR)

def handshake(msg,HANDSHAKE):
    if HANDSHAKE == True:
        return True
    if "MAC" in msg:
        MAC = msg.split(" ")[1]
        if MAC == "00:0c:29:51:c1:2f":
            print("HandShake Completo")
            return True
        else:
            return False

    

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    HANDSHAKE = False
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            print("Estoy leyendo: ",msg)
            # Decode base 64
            msg = base64.b64decode(msg).decode(FORMAT)

            if msg == DISCONNECT_MESSAGE:
                connected = False            
            HANDSHAKE = handshake(msg, HANDSHAKE)
            if HANDSHAKE == True:
                print(f"[{addr}] {msg}")
                conn.send("Msg received".encode(FORMAT))
            else:
                conn.send("Dispositivo desconocido".encode(FORMAT))

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
