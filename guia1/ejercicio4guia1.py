edad1 = input("Digite la edad 1: ")
edad2 = input("Digite la edad 2: ")

edad_1 = int(edad1)
edad_2 = int(edad2)

if edad_1>edad_2:
    print("La segunda persona es mas joven y tiene {} años ".format(edad_2))

elif(edad_1<edad_2):
    print("La primera persona es mas joven y tiene {} años ".format(edad_1))

else:
    print("Ninguno es mas joven, pues ambas edades son iguales")