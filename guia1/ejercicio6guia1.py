def arccot(x, unidad):
    suma = potenciax = unidad // x
    n = 3
    signo = -1
    while 1:
        potenciax = potenciax // (x * x)
        termino = potenciax // n
        if not termino:
            break
        suma += signo * termino
        signo = -signo
        n += 2
    return suma

def pi(digitos):
    unidad = 10 ** (digitos + 10)
    pi = 4 * (4 * arccot(5, unidad) - arccot(239, unidad))
    return pi // 10 ** 10

n=int(input("ingrese decimal de pi: "))
mipi=str(pi(n))
if n==0:
    print(mipi[:1])
else:
    mipi=mipi[:1]+"."+mipi[1:]
    print(mipi)