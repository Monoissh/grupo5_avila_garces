from math import pi

radio = float(input("Escriba el radio del circulo: "))
area = pi * radio**2
if radio <= 0:
    print("El valor del radio es incorrecto")
else:
    print("El area del circulo es: {0:.5f}".format(area))